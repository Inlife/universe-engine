-- dependency manager
if __dmanager == nil then

    __dmanager = { loaded = {} };
    function __dmanager.get(name) 
        return __dmanager.loaded[name];
    end;
    function __dmanager.empty(name)
        return (__dmanager.get(name) == nil);
    end;
    function __dmanager.add(name)
        __dmanager.loaded[name] = true;
    end;

end