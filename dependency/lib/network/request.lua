class "Request" {
    __init__ = function(self, data)
        self.data = data or {};
        self.signature = self:signature();
    end,

    signature = function(self)
        local set = {};
        set.a = tostring(self);
        set.b = tostring(self.data);
        set.a = set.a:gsub("table: ", "");
        set.b = set.b:gsub("table: ", "");

        self.signature = set.a .. set.b;
        return self.signature;
    end,

    getData = function(self)
        return self.data;
    end,

    run = function(self)
        triggerEvent("_ue_rqst", root, self);
    end,

    onResponse = function(self, callback)

        local function handler(response)
            -- If result origin not equal to caller - exit
            if (self.signature == response.request.signature) then
                removeEventHandler("_ue_resp", root, handler);
                callback(response);
            end;
        end;

        addEventHandler("_ue_resp", root, handler);
    end,
};