class "Response" (Request) {
    __init__ = function(self, data, request)
        Request.__init__(self, data);
        self.request = request or nil;
    end,

    run = function(self)
        triggerEvent("_ue_resp", root, self);
    end,
};