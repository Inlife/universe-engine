-- @author Inlife
-- @package Network
-- Request, Response

assert(loadstring(
    exports.dependency:import({
        "lib.network.request", 
        "lib.network.response"
    })
))();