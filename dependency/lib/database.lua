assert(loadstring(
    exports.dependency:import({
        "lib.network"
    })
))();

database = {};
database.query = function(query, callback)
    local request = Request({ destination = "database", query = query });

    request:onResponse(function(response)
        if callback then
            callback( response.data.result, response.data.affected, response.data.lastinserted);
        end
    end);

    return request:run();
end;