assert(loadstring(
    exports.dependency:import({
        "lib.database", 
        "lib.async"
    })
))();

class "EntityManager" {

    async = Async():setPriority("normal"):setDebug(true),
    
    __init__ = function(self, table, entity)
        self.table = table or "abstract";
        self._cache = {};
        self._index = {};
        self.__entity = entity;
    end,

    findOneBy = function(self, data, callback, target)
        local what = target or "*";
        local query = "SELECT " .. what .. " FROM " .. self.table .. self:where(data) .. " LIMIT 1";

        if self:index(query) then
            return callback( self:index(query) );
        end

        database.query(query, function(result)
            local object = self:convertSingle(result[1]);

            object = self:cache(object);
            self:index(query, object);

            callback(object);
        end);
    end,

    findBy = function(self, data, callback, target)
        local what = target or "*";
        local query = "SELECT " .. what .. " FROM " .. self.table .. self:where(data);

        if self:index(query) then
            return callback( self:index(query) );
        end

        database.query(query, function(results)
            self:convertMultiple(results, function(objects)
                objects:foreach(function(object)
                    object = self:cache(object);
                end);

                self:index(query, objects);

                callback(objects);
            end);
        end);
    end,

    findAll = function(self, callback, target)
        self:findBy({}, callback, target);
    end,

    save = function(self, object, callback)
        if (object:getId() > 0) then
            -- update
            if (object:__entityChanged()) then
                local query = "UPDATE " .. self.table .. " SET " .. object:__entityUpdates() .. " WHERE id=" .. object:getId();

                database.query(query, function()
                    if callback then callback(); end
                end);
            else
                if callback then callback(); end
            end
        else
            -- insert
            local query = "INSERT INTO " .. self.table .. " (" .. object:__entityKeys() .. ") VALUES (" .. object:__entityValues() .. ")";

            database.query(query, function(_a, _b, lastrow)
                object.data:set("id", lastrow);
                self:cache(object);

                if callback then callback(); end
            end);
        end
    end,

    remove = function(self, object, callback)
        local query = "DELETE FROM " .. self.table .. self:where({ id = object:getId() });
        
        database.query(query, function()
            object:__entityDestroy();
            if callback then callback(); end
        end);
    end,

    index = function(self, key, value)
        if (value) then
            if (type(value) == "number") then
                self._index[key] = value;
            elseif (class.isinstance(value, Entity)) then
                self._index[key] = value:getId(); -- self:cache(value:getId()):getId();
            elseif (class.isinstance(value, List)) then
                local indexes = List();
                value:foreach(function(object)
                    indexes:push( object:getId() );
                end);
                self._index[key] = indexes;
            else
                error("Manager.index: neither number nor entity");
            end
        else
            if (self._index[key]) then
                if (class.isinstance(self._index[key], List)) then
                    local objects = List();

                    self._index[key]:foreach(function(id)
                        if (not object:__entityDestroyed()) then
                            objects:push( self:cache(id) );
                        end
                    end);

                    return objects;
                else
                    local object = self:cache(self._index[key]);
                    if (not object:__entityDestroyed()) then
                        return object;
                    end
                end
            end
        end

        return nil; 
    end,

    cache = function(self, param)
        if (param == nil) then return nil; end
        if (type(param) == "number") then
            return self._cache[param];
        else
            if (param and param:getId() and self._cache[param:getId()] == nil) then
                --table.insert(self._cache, param:getId(), param);
                self._cache[param:getId()] = param;
                return param;
            else
                return self._cache[param:getId()];
            end
        end
    end,

    where = function(self, data)
        local count = 0
        for k,v in pairs(data) do
            count = count + 1;
        end

        if (count > 0) then
            local parts = {};

            for k,val in pairs(data) do
                if (type(val) == "string") then
                    val = "'" .. val .. "'";
                end
                -- table.insert(parts, k .. "=" .. val);
                parts[#parts + 1] = k .. "=" .. val;
            end

            return " WHERE " .. table.concat(parts, " AND ");
        else
            return "";
        end
    end,

    convertMultiple = function(self, data, callback)
        local objects = List();

        self.async:foreach(data, function(object)
            objects:push(
                self:convertSingle(object)
            );
        end, function()
            callback( objects );
        end);
    end, 

    convertSingle = function(self, data)
        return _G[self.__entity]():__entityInsert(data);
    end,
}