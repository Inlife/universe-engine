assert(loadstring(
    exports.dependency:import({
        "lib.core.class", 
        "lib.string.explode", 
        "lib.container", 
        "lib.entity.EntityManager"
    })
))();

class "Entity" {

    __manager = nil,
    __extended = false,
    __stored = Set({ "id" }),
    
    __init__ = function(self)
        self.data = Map({ id = 0 });
        self.modified = List();
        self.destoyed = false;
        self.changed = false;
    end,

    stored = function(self, data)
        if not __extended then
            __extended = true;
            self.__stored:push(data);
        end
    end,

    __entityInsert = function(self, data)
        for k,v in pairs(data) do
            if type(v) == "string" and v:match("json:") then
                local _,value = unpack(explode("json:", v));
                v = fromJSON(value); --json:decode(content);
            end

            self.data:set(k, v);
        end

        return self;
    end,

    __escapeValue = function(self, value)
        if (type(value) == "string") then
            value = "'" .. value .. "'";
        elseif (type(value) == "table") then
            value = "'json:" .. toJSON(value) .. "'"; --json:encode(value) .. "'";
        end

        return value;
    end,

    __entityKeys = function(self)
        return self.__stored:concat(",");
    end,

    __entityValues = function(self)
        local result = List();

        self.__stored:foreach(function(key)
            result:push(self:__escapeValue(
                self.data:get(key)
            ));
        end);

        return result:concat(",");
    end,

    __entityUpdates = function(self)
        local result = List();

        self.modified:foreach(function(key) 
            result:push(key .. "=" .. self:__escapeValue(
                self.data:get(key)
            ));
        end);

        self.modified = List();
        self.changed = false;

        return result:concat(",");
    end,

    __entityDestroy = function(self)
        self.destoyed = true;
    end,

    __isChanged = function(self)
        return self.changed;
    end,

    __isDestroyed = function(self)
        return self.destoyed;
    end,

    empty = function(self)
        return (self:getId() == 0 or self.destoyed == true);
    end,

    __getattr__ = function(self, key)
        if key:match("^set.+") then
            return function(self, data)
                local _,name = unpack(explode("set", key));
                name = name:lower();
                if (self.data:get(name) ~= data) then
                    if (self.__stored:exist(name)) then
                        self.modified:push(name);
                        self.changed = true;
                    end
                    self.data:set(name, data);
                end
                
                return self;
            end;
        elseif key:match("^get.+") then
            return function(self)
                local _,name = unpack(explode("get", key));
                return self.data:get(name:lower());
            end;
        else
            dbg(key);
            error("Non-existent entity.method : " .. key);
        end
    end,

    findOneBy = function(self, data, callback, target)
        return self.__manager:findOneBy(data, callback, target);
    end,

    findBy = function(self, data, callback)
        return self.__manager:findBy(data, callback, target);
    end,

    findAll = function(self, callback)
        return self.__manager:findAll(callback, target);
    end,

    save = function(self, callback)
        return self.__manager:save(self, callback);
    end,

    remove = function(self, callback)
        return self.__manager:remove(self, callback);
    end,

    index = function(self, name, object)
        return self.__manager:index(name, object);
    end,
}