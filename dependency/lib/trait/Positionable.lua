class "Positionable" {
    trait = function(self)
        
        self:stored({
            "px", "py", "pz"
        });

        self.data:push({
            px = 0, py = 0, pz = 0
        });
    end,

    position = function(self)
        return 42;
    end,

    setPosition = function(self, vector)
        self:setPX(vector.x);
        self:setPY(vector.y);
        self:setPZ(vector.z);
    end,

    getPosition = function(self)
        return Vector3(self:getPX(), self:getPY(), self:getPZ());
    end,
}