class "Rotationable" {
    trait = function(self)
        
        self:stored({
            "rx", "ry", "rz"
        });

        self.data:push({
            rx = 0, ry = 0, rz = 0
        });
    end,

    setRotation = function(self, vector)
        self:setRX(vector.x);
        self:setRY(vector.y);
        self:setRZ(vector.z);
    end,

    getRotation = function(self)
        return Vector3(self:getRX(), self:getRY(), self:getRZ());
    end,
}