if getResourceName(getThisResource()) ~= "dependency" then
    assert(loadstring(exports.dependency:import({"lib.container.interface"})))();
end

class "Map" (ContainerInterface)  {

    push = function(self, data)
        for key,value in pairs(data) do
            self.__data[key] = value;
        end
    end,
}