-- @author Inlife
-- @package Container
-- Set, List, Map

assert(loadstring(
    exports.dependency:import({
        "lib.container.interface", 
        "lib.container.set", 
        "lib.container.list", 
        "lib.container.map"
    })
))();