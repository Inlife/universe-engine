if getResourceName(getThisResource()) ~= "dependency" then
    assert(loadstring(exports.dependency:import({"lib.container.interface"})))();
end

class "Set" (ContainerInterface) {

    __init__ = function(self, ...)
        self.__index = {};
        ContainerInterface.__init__(self, ...);
    end,

    push = function(self, ...)
        local args = arg[1];
        if ( type(args) == "table" ) then
            for i = 1, #args do
                local value = args[i];
                self.__index[#self.__index + 1] = value;
                self.__data[value] = #self.__index;
            end
        else
            for i = 1, #arg do
                local value = arg[i];
                self.__index[#self.__index + 1] = value;
                self.__data[value] = #self.__index;
            end
        end
    end,

    remove = function(self, value)
        local id = self:get(value);
        self.__index[id] = nil;
        ContainerInterface.remove(self, value);
    end,

    foreach = function(self, callback)
        for k,v in pairs(self.__index) do
            callback(v, k);
        end
    end,

    set = function(self, value)
        self:push(value);
    end,
}