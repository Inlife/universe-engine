if getResourceName(getThisResource()) ~= "dependency" then
    assert(loadstring(exports.dependency:import({"lib.core.class"})))();
end

class "ContainerInterface" {

    __init__ = function(self, ...)
        self.__data = {};
        self:push(...);
    end,

    push = function(self)
    end,

    set = function(self, key, value)
        self.__data[key] = value;
    end,

    get = function(self, param)
        return self.__data[param];
    end,

    remove = function(self, key)
        self.__data[key] = nil;
    end,

    exist = function(self, param)
        return (self:get(param) ~= nil);
    end,

    exists = function(self, param)
        return self:exist(param);
    end,

    concat = function(self, symbol)
        local result = "";
        local first = true;

        self:foreach(function(value)
            if first then first = false;
                result = result .. value;
            else
                result = result .. symbol .. value;
            end
        end);

        return result;
    end,

    foreach = function(self, callback)
        for k,v in pairs(self.__data) do
            callback(v, k);
        end
    end,

    __getattr__ = function(self, value)
        return self:get(value);
    end,
}
