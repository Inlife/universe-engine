if getResourceName(getThisResource()) ~= "dependency" then
    assert(loadstring(exports.dependency:import({"lib.container.interface"})))();
end

class "List" (ContainerInterface) {

    push = function(self, ...)
        local args = arg[1];
        if ( type(args) == "table" and not args.__class__) then
            for i = 1, #args do
                local value = args[i];
                self.__data[#self.__data + 1] = value;
            end
        else
            for i = 1, #arg do
                local value = arg[i];
                self.__data[#self.__data + 1] = value;
            end
        end
    end,
}