if getResourceName(getThisResource()) ~= "dependency" then
    assert(loadstring(
        exports.dependency:import {"lib.class", "lib.map"}
    ))();
end

class "Switch" {
    __init__ = function(self)
        self.cases = Map();
        self.__default = false;
    end,

    case = function(self, condition, handler)
        if type(condition) == "table" then
            for i,v in ipairs(condition) do
                self.cases:set(v, handler);
            end 
        else
            self.cases:set(condition, handler);
        end

        return self;
    end,

    default = function(self, handler)
        self.__default = true;
        self.cases:set("__default", handler);

        return self;
    end,

    test = function(self, value)
        if self.cases:get(value) then
            self.cases:get(value)();
        else
            if self.__default then
                self.cases:get("__default")();
            end
        end

        return self;
    end,
}