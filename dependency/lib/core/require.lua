if getResourceName(getThisResource()) ~= "dependency" then
    assert(loadstring(
        exports.dependency:import {"lib.set"}
    ))();
end


require = function(arguments)
    local input = Set(arguments);
    local files = Set();
    
    input:foreach(function(filename)
        filename = filename:gsub('%.', "/");
        filename = filename .. ".lua";

        files:push(filename);
    end);

    files:foreach(function(filename)
        if fileExists(filename) then
            local file = fileOpen(filename);

            local code, errmsg = loadstring(fileRead(file, fileGetSize(file)), filename);
            if (code) then
                local status, errmsg = pcall(code); 
                if (not status) then
                    error("(import) " .. errmsg, 1);
                end
            else
                error("(import) " .. errmsg, 0);
            end
        else
            outputServerLog("Error @ 'require' [File not found!]: " .. filename , 2)
        end
    end);
end