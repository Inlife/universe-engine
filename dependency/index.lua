-- add global dependant events
addEvent("_ue_rqst", true);
addEvent("_ue_resp", true);
addEvent("_ue_import_dbg", true);

local __dcache = Map({});
local __dmeta = {
    count = 0,
    log = {}
};

addEventHandler("_ue_import_dbg", root, function()
    dbgf(__dmeta);
end);

-- main import function
function import(list)

    local defaults = Set("manager", "lib.string.printr");
    local files = Set();
    local code = "";


    local function convert(filename)
        filename = filename:gsub('%.', "/");
        filename = filename:gsub('lib/core/class', "lib/core/slither");
        filename = filename:gsub('.lua', "");
        filename = filename .. ".lua";

        return filename;
    end;

    local function package(filename)
        filename = filename:gsub('.lua', "");
        filename = filename .. "/package.lua";

        return filename;
    end;

    local function cache(filename)
        if __dcache:exists(filename) then
            return __dcache:get(filename);
        else
            local file = fileOpen(filename);
            local content = fileRead(file, fileGetSize(file));
            fileClose(file);

            __dmeta.count = __dmeta.count + 1;
            __dmeta.log[#__dmeta.log + 1] = filename;

            __dcache:set(filename, content);

            return content;
        end
    end;

    local function chunk(filename)
        local chunk = "if not __dmanager or __dmanager.empty(\"".. filename .."\") then\n";
        chunk = chunk .. cache(filename);
        chunk = chunk .. "\n__dmanager.add(\"".. filename .."\"); end";

        return chunk;
    end;

    defaults:foreach(function(filename)
        files:push( convert(filename) );
    end);

    for i = 1, #list do
        local filename = list[i];
        filename = convert(filename);

        if (not files:exist(filename)) then
            files:push(filename);
        end
    end

    files:foreach(function(filename)
        local packaged = package(filename);

        if fileExists(filename) then
            code = code .. "\n\r\n\r" .. chunk(filename);
        elseif fileExists(packaged) then
            code = code .. "\n\r\n\r" .. chunk(packaged);
        else
            outputServerLog("File or package doesn't exist: " .. filename);
        end
    end);

    dbgf(code);

    return code;
end