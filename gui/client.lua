function print_r(t, name, indent)
     local cart     -- a container
     local autoref  -- for self references

     --[[ counts the number of elements in a table
     local function tablecount(t)
            local n = 0
            for _, _ in pairs(t) do n = n+1 end
            return n
     end
     ]]
     -- (RiciLake) returns true if the table is empty
     local function isemptytable(t) return next(t) == nil end

     local function basicSerialize (o)
            local so = tostring(o)
            if type(o) == "function" then
                 local info = debug.getinfo(o, "S")
                 -- info.name is nil because o is not a calling level
                 if info.what == "C" then
                        return string.format("%q", so .. ", C function")
                 else 
                        -- the information is defined through lines
                        return string.format("%q", so .. ", defined in (" ..
                                info.linedefined .. "-" .. info.lastlinedefined ..
                                ")" .. info.source)
                 end
            elseif type(o) == "number" or type(o) == "boolean" then
                 return so
            else
                 return string.format("%q", so)
            end
     end

     local function addtocart (value, name, indent, saved, field)
            indent = indent or ""
            saved = saved or {}
            field = field or name

            cart = cart .. indent .. field

            if type(value) ~= "table" then
                 cart = cart .. " = " .. basicSerialize(value) .. ";\n"
            else
                 if saved[value] then
                        cart = cart .. " = {}; -- " .. saved[value] 
                                                .. " (self reference)\n"
                        autoref = autoref ..  name .. " = " .. saved[value] .. ";\n"
                 else
                        saved[value] = name
                        --if tablecount(value) == 0 then
                        if isemptytable(value) then
                            if value.__class__ then
                                cart = cart .. " = <" .. value.__name__ .. "> {};\n"
                            else 
                                cart = cart .. " = {};\n"
                            end
                        else
                            if value.__class__ then
                                cart = cart .. " = <" .. value.__name__ .. "> {\n"
                            else 
                                cart = cart .. " = {\n"
                            end
                            for k, v in pairs(value) do
                                k = basicSerialize(k)
                                local fname = string.format("%s[%s]", name, k)
                                field = string.format("[%s]", k)
                                -- three spaces between levels
                                addtocart(v, fname, indent .. "   ", saved, field)
                            end
                            cart = cart .. indent .. "};\n"
                        end
                 end
            end
     end

     name = name or "__unnamed__"
     if type(t) ~= "table" then
            return name .. " = " .. basicSerialize(t)
     end
     cart, autoref = "", ""
     addtocart(t, name, indent)
     return cart .. autoref
end

function dbg(a)outputConsole(print_r(a))end


local fps = false
function getCurrentFPS() -- Setup the useful function
    return fps
end
 
local function updateFPS(msSinceLastFrame)
    -- FPS are the frames per second, so count the frames rendered per milisecond using frame delta time and then convert that to frames per second.
    fps = (1 / msSinceLastFrame) * 1000
end
addEventHandler("onClientPreRender", root, updateFPS)

local sx = guiGetScreenSize()
local function drawFPS()
    if not getCurrentFPS() then
        return
    end
    local roundedFPS = math.floor(getCurrentFPS())
    dxDrawText(roundedFPS, sx - dxGetTextWidth(roundedFPS), 0)
end
addEventHandler("onClientHUDRender", root, drawFPS)

function PlotAntiAliasedPoint(pixels, x, y, r, g, b, a)
    local prox = 0.9;
    for roundedx = math.floor(x), math.ceil(x), prox do
        for roundedy = math.floor(y), math.ceil(y), prox do
            local percent_x = math.abs ( x - roundedx );
            local percent_y = math.abs ( y - roundedy );
            local percent = percent_x * percent_y;

            dxSetPixelColor(pixels, roundedx, roundedy, r, g, b, a * percent);
        end
    end
end
    
function generateSegment(angle1, angle2, radius, width, r, g, b, a, prox)
    local size = radius * 2;
    local proximity = prox or 0.1;
    local radius2 = radius - width;
    local offset = 0.5;

    local a1 = angle1 + 180;
    local a2 = angle2 + 180;

    local floor,rad = math.floor, math.rad;
    local sin,cos = math.sin, math.cos;

    local texture = dxCreateTexture(size, size);
    local pixels = dxGetTexturePixels(texture);

    for angle = a1, a2, proximity do
        for curradius = radius2, radius do
            local x = (cos(rad(angle)) * curradius);
            local y = (sin(rad(angle)) * curradius);

            PlotAntiAliasedPoint(pixels, x + radius, y + radius, r, g, b, a);
            if curradius < radius - offset and curradius > radius2 + offset then
                if (angle > a1 - offset and angle < a2 - offset - 0.1) then
                    dxSetPixelColor(pixels, x + radius, y + radius, r, g, b, a);
                end
            end
        end
    end

    dxSetTexturePixels(texture, pixels);

    return texture;
end

function inSegment(cx, cy, fx, fy, r1, r2, a1, a2)
    local dist = math.sqrt((fx - cx)^2 + (fy - cy)^2);
    if dist > r1 and dist < r2 then
        local angle = math.atan2(fy - cy, fx - cx) * 180 / math.pi + 180;
        dbg(angle);
        if angle > a1 and angle < a2 then
            return true;
        end
    end
    return false;
end

local scrx, scry = guiGetScreenSize();

local opened = false;
local items = 8;
local space = 2;
local selected = -1;

local clean = 360 / items;
local angle = (360 / items) - space;

local texture = generateSegment(0, angle, 256, 100, 255, 255, 255, 255);
local myWindow = guiCreateWindow(0, 0, scrx, scry, "", true);
guiSetAlpha(myWindow, 0.5);
guiSetVisible(myWindow, false);


addEventHandler("onClientRender", getRootElement(), function()
    if opened then
        -- dxDrawRectangle(0, 0, scrx, scry, tocolor(0, 0, 0, 100 ));
        for i = 0, items - 1 do
            local alpha = 70;
            if (i == selected) then
                alpha = 175;
            end
            dxDrawImage(300, 300, 256, 256, texture, i * clean + space / 2, 0, 0, tocolor(255, 255, 255, alpha), true)
        end
    end
end);

addEventHandler("onClientMouseMove", getRootElement(), function(x, y)
    -- dbg(x .. " - " .. y);
    local sel = false;
    for i = 0, items do
        if inSegment(428, 428, x, y, 128 - 50, 128, clean * i, clean * i + angle) then
            selected = i;
            sel = true;
        end
    end
    if not sel then
        selected = -1;
    end
end);


bindKey("e", "down", function(player)
    opened = not opened;
    guiSetVisible(myWindow, opened);
end);