addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), function() 
    
    local resources = {
        "dependency",
        "database",
        "account",
        "ivory"
    };

    for i = 1, #resources do
        local name = resources[i];
        local res = getResourceFromName(name);
        outputServerLog("[init] Starting: " .. name);

        if res then
            startResource(res);
            outputServerLog("[init] Started: " .. name);
        else
            outputServerLog("[init] Failed: " .. name);
        end
    end

end);