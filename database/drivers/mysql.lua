assert(loadstring(
    exports.dependency:import({
        "lib.core.class", 
        "lib.network"
    })
))();

class "Mysql" {
    __init__ = function(self, username, password, dbname, host, port)
        self.username = username or get("user");
        self.password = password or get("pass");
        self.dbname = dbname or get("name");
        self.host = host or get("host");
        self.port = port or get("port");
        self.connected = false;

        local type = self:configureType();
        self.entity = dbConnect("mysql", type, self.username, self.password);
    end,

    configureType = function(self)
        return table.concat({
            "dbname=" .. self.dbname,
            "host=" .. self.host,
            "port=" .. self.port
        }, ";");
    end,

    query = function(self, cbfunc, query, ...)
        dbQuery(function(qh)
            cbfunc( dbPoll( qh, 0 ) ); 
        end, self.entity, query, ...);
    end,

    exec = function(self, query, ...)
        return dbExec(self.entity, query, ...);
    end,

    close = function(self)
        -- dbClose(self.entity);
    end,
}