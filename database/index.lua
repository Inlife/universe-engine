local _connection = Mysql();

addEventHandler("_ue_rqst", root, function(request)
    -- check if request destination matches
    if (request.data.destination == "database") then
        _connection:query(function(result, affected, lastinserted)
            Response({result = result, affected = affected, lastinserted = lastinserted}, request):run();
        end, request.data.query);
    end
end);