 
local sx, sy = guiGetScreenSize ()
 
addEventHandler("onClientPreRender", getRootElement(),
function()
        if getElementHealth(getLocalPlayer()) >= 1 then
            local lx, ly, lz = getWorldFromScreenPosition ( sx/2, sy/2, 10 )
            setPedAimTarget(localPlayer, lx, ly, lz)
            setPedLookAt(localPlayer, lx, ly, lz)
            triggerLatentServerEvent("moveHeadSync",500,getLocalPlayer(),lx,ly,lz)
        end
end)
 
 
function rotateHead(player,x,y,z)
	setPedAimTarget(player, x,y,z)
    setPedLookAt(player, x,y,z)
end
addEvent("rotateHead",true)
addEventHandler("rotateHead",getRootElement(),rotateHead)
 
addEventHandler("onTestEvent", getRootElement(), function(data)
    outputServerLog("test");
end)