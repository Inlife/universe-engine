assert(loadstring(
    exports.dependency:import {"lib.core.class", "lib.entity.Entity"}
))();

class "Organization" (Entity) {

	__manager = EntityManager("organizations", "Organization"),

    __init__ = function(self)
    	Entity.__init__(self);

    	self.data:push({
	        title = "unnamed",
	        shorttitle = "unnamed",

	        owner = 0,

	        parent = 0
    	})

    	self:stored({
    		"title", "shorttitle", 
    		"owner", "parent"
    	});
        
        self:registerEvents();
    end,

}