class "Relation"	(Entity) {

	__manager = EntityManager("relations", "Relation"),

    __init__ = function(self)
    	Entity.__init__(self);

    	self.data:push({
	        first = 0,
	        second = 0,

	        state = "none", -- request, friends, blocked
    	})

    	self:stored({
    		"first", "second", "state"
    	});
    end

}