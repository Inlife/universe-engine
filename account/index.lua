assert(loadstring(
    exports.dependency:import {"lib.core.class", "lib.entity.Entity"}
))();

addEvent("onCustomEv", true)

class "Account"	(Entity) {

	__manager = EntityManager("accounts", "Account"),

    __init__ = function(self)
    	Entity.__init__(self);

    	self.data:push({
	        login = "unnamed",
	        password = "unknown",

	        state = "offline", -- online, offline, frozen, banned

	        nickname = "unnamed",
	        rank = 0,
	        adminstate = 0,
            experience = 0;

	        settings = Map()
    	})

    	self:stored({
    		"login", "password", "state",
    		"nickname", "rank", "adminstate",
    		"settings"
    	});
        
        self:registerEvents();
    end,

    registerEvents = function(self)
        addEventHandler("onPlayerJoin", root, function() self:onJoin(); end);
        addEventHandler("onPlayerQuit", root, function(quitType) self:onQuit(quitType); end);
        addEventHandler("onCustomEv", root, function() self:onCustom(); end);
    end,

    onCustom = function()
        dbg(source);
    end,

    onLogin = function(self, account)
        -- body
    end;

    onJoin = function(self)
        -- TriggerClientEvent and show GUI
        triggerClientEvent(source, "form:loginWindow", source);
        dbg(getPlayerName(source));
        dbg(self);
        dbg(source);
    end,

    onQuit = function(self, quitType)
        -- Save account and clear cache
        dbg(quitType);
    end,

    setLevel = function(self, lvl)
        self:setExperience(lvl * 100);
        return self;
    end,

    getLevel = function(self)
        return math.floor(self:getExperience() / 100);
    end,

    getSettings = function(self, key)
        return self.data.settings:get(key);
    end,

    setSettings = function(self, key, value)
        self.data.data:set(key, value);
        self.modified:push("settings");
        self.changed = true;
        return self;
    end,

    removeSettings = function(self, key)
        self.data.settings:remove(key);
        self.modified:push("settings");
        self.changed = true;
        return self;
    end,

}

--[[

local constant = 1000;
local base = 1.2;

for level = 0, 20 do
    local result = constant / (base ^ level); 
    print("Level #" .. level .. ": " .. result);
end

]]