assert(loadstring(
    exports.dependency:import {"lib.core.class", "lib.entity.Entity"}
))();

class "Character" (Entity) {
	
    __manager = EntityManager("characters", "Character"),

	__init__ = function(self)
        Entity.__init__(self);

        self.data:push({
            owner = 0,
            created = 0,
            active = false,
            timeplayed = 0,
            name = "unnamed",
            health = {
                status = 0
                blood = 0.0,
                bloodtype = 0,
                --[[
                stomach = 0.0,   -- From 0 to 4000 
                water = 0.0,     -- From 0 to 4000
                energy = 0.0,    -- From 0 to 20 000
                shock = 0,
                ]]
            },
            appearance = {
                gender = 0,
                skin = 0,
                skincolor = 0,
                age = 0,
                weight = 0.0,
                height = 0.0,
                walkstyle = 0
            },
            x = 0.0,
            y = 0.0,
            z = 0.0,
            r = 0,
            interior = 0,
            dimension = 0,
            data = {}
        });

        self:stored({
            "owner", "created", "lastlogin", "timeplayed",
            "name", "health", "appearance", "x", "y", "z", "r", "interior",
            "dimension", "data"
        });

		self:registerEvents();
	end,

	registerEvents = function(self)
		--addEventHandler("onPlayerJoin", root, function() self:onJoin(); end);
		--addEventHandler("onPlayerQuit", root, function(quitType) self:onQuit(quitType); end);
	end,

	load = function(self)
		-- loading with Account Manager
	end,

	save = function(self)
		-- saving with Account Manager
	end,

    setPosition = function(self, vector)
        self:setX(vector.x);
        self:setY(vector.y);
        self:setZ(vector.z);
        self:setR(vector.r or 0);
        return self;
    end,

    getPosition = function(self)
        return Vector4(self:getX(), self:getY(), self:getZ(), self:getR());
    end,
    --[[
    setBlood = function(self, blood)
        self:setBlood(blood or self:getWeight() * math.random(70, 75));
        return self;
    end,
    ]]
    getFingerPrint = function(self)
        return hash("md5", self:getName());
    end,

}