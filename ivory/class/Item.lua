-- import libraries
assert(loadstring(
    exports.dependency:import({
        "lib.core.class", 
        "lib.trait.Positionable", 
        "lib.trait.Rotationable", 
        "lib.entity.Entity"
    })
))();

-- Override EntityManager for extended convertation
-- Creating entity objects from type
class "ItemManager" (EntityManager) {
    
    __init__ = function(self)
        EntityManager.__init__(self, "items", "Item");
    end,

    convertSingle = function(self, data)
        return _G[data.type]():__entityInsert(data);
    end,
}

class "Item" (Positionable, Rotationable, Entity) {
    
    __manager = ItemManager(),

    __init__ = function(self, data)
        Entity.__init__(self);

        Positionable.trait(self);
        Rotationable.trait(self);

        self:stored({
            "state", "parent", "data", "type"
        });

        self.data:push({
            type = self.__name__,
            state = "world",
            title = "Default item",
            description = "Default item, default item, default item.",

            model = 1577, -- default item model id
            parent = 0,
            container = 0,
            volume = 0,

            removable = true,

            instance = nil,
            collision = nil,
            radius = 1,

            data = Map({}),
        });

        self.data:push(data or {});
    end,


    -- <Map> self.data methods

    getData = function(self, key)
        return self.data.data:get(key);
    end,

    setData = function(self, key, value)
        self.data.data:set(key, value);
        self.modified:push("data");
        self.changed = true;
        return self;
    end,

    removeData = function(self, key)
        self.data.data:remove(key);
        self.modified:push("data");
        self.changed = true;
        return self;
    end,


    -- Game entity methods

    create = function(self)

        if (self:getState() == "world") then
            self:setInstance(createObject(
                self:getModel(), 
                self:getPx(), self:getPy(), self:getPz(),
                self:getRx(), self:getRy(), self:getRz()
            ));

            addEventHandler("onElementClicked", self:getInstance(), function(...) self:onClick(unpack(arg)) end);
        end
        
        -- if (self:getState() == "world" or self:getState() == "fakeworld") then

        --     self:setCollision(createColSphere(
        --         self:getPosition().x,
        --         self:getPosition().y,
        --         self:getPosition().z,
        --         self:getRadius()
        --     ));

        --     addEventHandler("onColShapeHit", self:getCollision(), function(player) self:onCollisionEnter(player); end);
        --     addEventHandler("onColShapeLeave", self:getCollision(), function(player) self:onCollisionLeave(player); end);

        -- end

        return self;
    end,

    destroy = function(self)
        self:__destroyEntity();
    end,


    -- Events

    onCollisionEnter = function(self, source)
        dbg(getPlayerName(source));
    end,

    onCollisionLeave = function(self, source)
        dbg("player left");
    end,

    onClick = function(self, button, state, player)
        dbg("item clicked: " .. button .. " state: " .. state);
    end

}